

	<div id="menu">
       		<ul>
		        <li>
					<?php if(null != $this->session->userdata('is_logged_in')) { 
					 			echo anchor('login/logout', 'LOG OUT');
						    }else {
			    			    echo anchor('login', 'LOG IN'); 
			        		}
			        ?>
			    </li>                      
			</ul> 

	</div>
	<div id="blog">

		<?php $counter = 0 ?>
		<?php if(isset($records)) : foreach($records as $row) : ?>
	  <div class="container">		
		<?php $counter += 1 ?>
		<?php  if($counter % 2 == 0) :?> 	
		<div class="bubble">
		<?php else : ?>		
		<div class="bubble2">	
		<?php endif; ?>
			<div class="left delete">
				<?php if(null != $this->session->userdata('is_logged_in')) { 
						echo anchor("guestbook/delete/$row->id", '<img src="'.base_url().
						'images/gnome_edit_delete.ico" alt="Delete" />'); 
						}?>


			
			</div>
			<div class="right">
				<h2><?php echo '<span>'.$row->name. '</span>'.' on '.$row->timestamp; ?> </h2>
						<hr />
				<p><?php echo $row->message; ?></p>
			</div>
		</div>	
	  </div>
			<?php endforeach; ?>
			<?php endif; ?>
		<hr />
				<div class="alert"><?php echo validation_errors(); ?> </div>
			<?php $attributes = array('class' => 'message_form'); ?>
			<?php echo form_open('guestbook/create',$attributes);?>
			<h1>Guestbook Form
				<span>Feel free to leave your comment.</span>
			</h1>
			<p>
				<label for="name">
					<span>Name:</span>
					<input type="text" name="name" id="name" placeholder="Your Name" />
				</label>
			</p>
			<p>
				<label for="message">
					<span>Message:</span>
					<input type="textarea" name="message" id="message" placeholder="Your Comment" />
				</label>
			</p>	
			<p>
				<input type="submit" class="button" value="Submit" />
			</p>
			<?php echo form_close(); ?>

	</div>