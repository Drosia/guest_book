<?php  

Class Guestbook extends CI_Controller {

	function index()
	{
		$data = array();
		
		if($query = $this->guestbook_model->get_messages())
		{
			$data['records'] = $query;
		}
		$data['main_content'] = 'guestbook_view';
		$this->load->view('includes/template', $data);

	}

	function create()
	{


		$this->form_validation->set_rules('name', 'name', 'trim|required|min_length[3]|max_length[8]|xss_clean');
		$this->form_validation->set_rules('message', 'message', 'trim|required|min_length[5]|max_length[150]|xss_clean');
		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}else{
		
				$data = array(
					'name' => $this->input->post('name'),
					'message' => $this->input->post('message')
				);
				
				$this->guestbook_model->add_messages($data);
				redirect('guestbook');
			  }	
	}
	function delete(){
		$this->guestbook_model->delete_messages();
		redirect('guestbook');	
	}

}

?>

