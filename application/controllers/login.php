<?php

class Login extends CI_Controller {
	
	function index()
	{	
		$data = array();
		$data['main_content'] = 'login_form';
		$this->load->view('includes/template', $data);		
	}
	
	function validate_credentials()
	{	

		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[10]|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
			return ;
		}
	
		$this->load->model('admin_model');
		$query = $this->admin_model->validate();
		
		if(isset($query)) // if the user's credentials validated...
		{
			$data = array(
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('guestbook');
		}
		else // incorrect username or password
		{
			$this->index();
		}
	}	
	
	function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}

}