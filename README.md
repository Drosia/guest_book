HAE GROUP - WEB DEVELOPER TEST V1.0


========================

GUESTBOOK

-------------

## Installation

 

*First , Create a sql database with the name "Guestbook", then import the Guestbook.sql file into this database:

Or use 
~~~~


CREATE TABLE IF NOT EXISTS `Messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `timestamp` varchar(20) NOT NULL DEFAULT 'CURRENT_TIMESTAMP(6)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

INSERT INTO `Messages` (`id`, `name`, `message`, `timestamp`) VALUES
(66, 'Bob', 'This is the first Comment !', 'Wed, 02 Dec 15 11:57'),
(67, 'John', 'I really enjoyed your service''s', 'Wed, 02 Dec 15 11:58'),
(68, 'Lorum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis', 'Wed, 02 Dec 15 11:59'),
(69, 'Maria', 'This comment is awesome !!!', 'Wed, 02 Dec 15 12:00');

CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `Users` (`id`, `user`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');
~~~~ 



 

*Then move all the files into a folder named guestbook folder and then into a apache server (http folder) .

*If you want different name for the folder and not `guestbook` go to application\config\config.php file and change 

 $config['base_url'] = 'http://web_server_ip/you_name_the_folder/'; line as provided .
 

*Last step to make the web application to work , you need to go to application\config\database.php file and change 
those lines  


+If you have default localhost sql server with root permitions and blank password please skip last step . 
~~~~ 
   $db['default']['hostname'] = 'localhost';
   $db['default']['username'] = 'root';
   $db['default']['password'] = '';
   $db['default']['database'] = 'Guestbook';
~~~~ 
 

------



## Usage

------

*Open a browser and type http:/ip_of_the_webserver/nameofthefolder/ 'http://localhost/guestbook/' and enjoy the web application .

*For Login as Administrator Use Credentials below


Username : admin


Password : admin

-Cheers !

